<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Peran extends Model
{
    use HasFactory;

    public function film()
    {
        return $this->belongsToMany(Film::class);
    }

    public function cast()
    {
        return $this->hasMany(cast::class);
    }

}
