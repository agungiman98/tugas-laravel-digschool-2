<?php

namespace App\Http\Controllers;

use App\Models\Auth;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $data = Auth::all();
        return view('register');
    }

    public function welcome(Request $data)
    {
        $fname = $data['fname'];
        $lname = $data['lname'];
        return view('halo', compact('fname', 'lname'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        // ['fname' => $data['fname'],
        // 'lname' => $data['lname'],
        // 'gender' => $data['gender'],
        // 'nat' => $data['nat'],
        // 'langspok' => $data['langspok'],
        // 'bio' => $data['bio']  ]
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
        'fname' => 'required',
        'lname' => 'required',
        'gender' => 'required',
        'nat' => 'required',
        'langspok' => 'required', 
        'bio' => 'required']);
        Auth::create($validated);
        dd($validated);
        // return redirect()->route('register');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Auth  $auth
     * @return \Illuminate\Http\Response
     */
    public function show(Auth $auth)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Auth  $auth
     * @return \Illuminate\Http\Response
     */
    public function edit(Auth $auth)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Auth  $auth
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Auth $auth)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Auth  $auth
     * @return \Illuminate\Http\Response
     */
    public function destroy(Auth $auth)
    {
        //
    }
}
