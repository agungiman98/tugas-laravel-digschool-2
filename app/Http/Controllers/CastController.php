<?php

namespace App\Http\Controllers;

use App\Models\cast;
use Illuminate\Auth\Events\Validated;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = cast::all();
        return view('cast', ['dataCast' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('cast-create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate(['nama' => 'required',
                            'umur' => 'required',
                            'bio' => 'required']);
        // $cast = new cast(['nama' => $request->get('nama'),
        // 'umur' => $request->get('umur'),
        // 'bio' => $request->get('bio')]);
        // $cast->save();
        cast::create($validated);
        return redirect()->route('cast.index')->with('completed', 'Sukses Menambahkan Data Cast!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\cast  $cast
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = cast::find($id);
        return view('cast-detail', [
            'id' => $data
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\cast  $cast
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = cast::find($id);
        return view('cast-edit', [
            'id' => $data
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\cast  $cast
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $request->validate(['nama' => 'required',
                            'umur' => 'required',
                            'bio' => 'required']);
        $id = cast::find($id);
        $id->nama = $request->nama;
        $id->umur = $request->umur;
        $id->bio = $request->bio;
        $id->update();
        return redirect()->route('cast.index')->with('completed', 'Sukses Update Data Cast!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\cast  $cast
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $id = cast::find($id);
        $id->delete();
        return redirect()->route('cast.index');
    }
}
