<?php

use Illuminate\Contracts\View\View;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\IndexController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [IndexController::class, 'index'])->name('home')->middleware('auth');

// Route::get('/register-lawas', [AuthController::class, 'index']);
Route::post('/halo',[AuthController::class, 'welcome'])->middleware('auth');

Route::get('/data-tables', [IndexController::class, 'dataShow'])->name('dataTables')->middleware('auth');
;

Route::get('/cast', [CastController::class, 'index'])->name('cast.index')->middleware('auth');
Route::get('/cast/create', [CastController::class, 'create'])->middleware('auth');
Route::post('/cast', [CastController::class, 'store'])->name('cast.store')->middleware('auth');

Route::get('cast-detail/{id}', [CastController::class, 'show'])->name('cast.show')->middleware('auth');
;

Route::get('cast-edit/{id}', [CastController::class, 'edit'])->name('cast.edit')->middleware('auth');

Route::put('cast/{id}', [CastController::class, 'update'])->name('cast.update')->middleware('auth');

Route::delete('cast/{id}/destroy', [CastController::class, 'destroy'])->name('cast.destroy')->middleware('auth');



Auth::routes();

Route::get('/home', [HomeController::class, 'index'])->name('home')->middleware('auth');
