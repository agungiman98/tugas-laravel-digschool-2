@extends('layouts.master')
@section('title', 'Daftar Cast')
@section('content')

<table class="table table-bordered">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
        <th scope="col">Umur</th>
        <th scope="col">Bio</th>
        <th scope="col">Edit</th>
      </tr>
    </thead>

        
    <tbody>
      <tr>
        <th scope="row">{{ $id->id }}</th>
        <td>{{ $id->nama }}</td>
        <td>{{ $id->umur }}</td>
        <td>{{ $id->bio }}</td>
        <td>
          <a href="{{ route('cast.edit', $id->id )  }}" class="badge badge-primary">Edit</a>
          <form action="{{ route('cast.destroy', $id->id ) }}" method="post">
            @csrf
            @method('DELETE')
            <button type="submit" class="badge badge-danger">Hapus</button>
          </form>
        </td>
      </tr>
    </tbody>
        

  </table>

@endsection