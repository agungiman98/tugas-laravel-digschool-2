@extends('layouts.master')
@section('title', 'Daftar Cast')
@section('content')

<form action="{{ route('cast.update', $id->id) }}" method="post" class="m-4" name="formcast">
    @csrf
    @method('PUT')
    <div class="form-group">
      <label for="InputNama">Nama :</label>
      <input type="text" class="form-control" id="InputNama" aria-describedby="emailHelp" name="nama"
       value="{{ $id->nama }}">
    </div>
    <div class="form-group">
      <label for="umur">Umur :</label>
      <input type="number" class="form-control" id="umur" min="1" max="100" name="umur" value="{{ $id->umur }}">
    </div>
    <div class="form-group">
      <label for="bioCast">Bio</label>
      <textarea class="form-control"  id="BioCast" cols="30" rows="10" name="bio">{{ $id->bio }}</textarea >
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
    <a class="btn btn-danger m-2" href="/cast" role="button">Cancel</a>
  </form>

@endsection