@extends('layouts.master')
@section('title', 'Daftar Cast')
@section('content')

<a class="btn btn-success m-4" href="/cast/create" role="button">Tambah Data</a>

<table class="table table-bordered">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
        <th scope="col">Umur</th>
        <th scope="col">Bio</th>
        <th scope="col">Edit</th>
      </tr>
    </thead>

        
    <tbody>
      @foreach ($dataCast as $d)
      <tr>
        <th scope="row">{{ $d->id }}</th>
        <td>{{ $d->nama }}</td>
        <td>{{ $d->umur }}</td>
        <td>{{ $d->bio }}</td>
        <td>
          @guest
              
          @endguest
          <a href="{{ route('cast.edit', $d->id ) }}" class="badge badge-primary">Edit</a>
          <a href="{{ route('cast.show', $d->id )  }}" class="badge badge-success">detail</a>
          <form action="{{ route('cast.destroy',$d->id) }}" method="post">
            @csrf
            @method('DELETE')
            <button type="submit" class="badge badge-danger">Hapus</button>
          </form>
        </td>
      </tr>
      @endforeach
    </tbody>
        

  </table>

@endsection